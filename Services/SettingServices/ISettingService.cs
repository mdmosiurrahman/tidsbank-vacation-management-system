using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.SettingsDTO;

namespace TidsbankenFinalProject.Services.SettingServices
{
    public interface ISettingService
    {
        public Task<IEnumerable> GetAllSettingsAsync();
        public Task<Setting> EditSettingsAsync(Setting setting);

        public Task<Setting> CreateSetting(Setting setting);
        public Task<Setting> DeleteSettingsByIdAsync(int Id);
        public Task<Setting> GetSettingsByIdAsync(int Id);
        public bool SettingExist(int id);

    }
}
