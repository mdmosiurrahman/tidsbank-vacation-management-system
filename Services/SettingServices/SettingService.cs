using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.SettingsDTO;

namespace TidsbankenFinalProject.Services.SettingServices
{
    public class SettingService : ISettingService
    {

        private readonly TidsBankDBContext _context;
        private readonly IMapper _mapper;

        public async Task<Setting> GetSettingsByIdAsync(int Id) => await _context.Settings.SingleOrDefaultAsync(u => u.Id == Id);
        public SettingService(TidsBankDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable> GetAllSettingsAsync()
        {
            return await _context.Settings.ToArrayAsync();
        }

        public async Task<Setting> EditSettingsAsync(Setting setting)
        {
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return setting;
        }

        public async Task<Setting> DeleteSettingsByIdAsync(int Id)
        {
            var setting = await GetSettingsByIdAsync(Id);
            if (setting == null) return null;

            _context.Settings.Remove(setting);
            await _context.SaveChangesAsync();
            return setting;
        }

        public bool SettingExist(int id)
        {
            return _context.Settings.Any(s => s.Id == id);
        }

        public async Task<Setting> CreateSetting(Setting setting)
        {
            await _context.Settings.AddAsync(setting);
            await _context.SaveChangesAsync();
            return setting;
        }
    }
}
