﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.IneligiblePeriods;

namespace TidsbankenFinalProject.Services.IneligiblePeriodServices
{
    public class IneligiblePeriodService : IIneligiblePeriodService
    {

        private readonly TidsBankDBContext _context;

        public IneligiblePeriodService(TidsBankDBContext context)
        {
            _context = context;
        }


        public async Task<IneligiblePeriod> CreateIneligiblePeriodAsync(IneligiblePeriod ip)
        {
            await _context.IneligiblePeriods.AddAsync(ip);
            await _context.SaveChangesAsync();
            return ip;
        }

        public async Task<IEnumerable<IneligiblePeriod>> GetAllIneligiblePeriodAsync()
        {
            return await _context.IneligiblePeriods.Include(i => i.User).ToArrayAsync();
        }

        public async Task<IneligiblePeriod> GetIneligiblePeriodByIdAsync(int ipId)
        {
            return await _context.IneligiblePeriods.Include(i => i.User).SingleOrDefaultAsync(ip => ip.Id == ipId);

        }

        public async Task<IneligiblePeriod> UpdateIneligiblePeriodAsync(IneligiblePeriod ip)
        {
            _context.Entry(ip).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return ip;
        }

        public async Task<IneligiblePeriod> DeleteIneligiblePeriodAsync(int Id)
        {
            var ip = await GetIneligiblePeriodByIdAsync(Id);
            if (ip == null) return null;

            _context.IneligiblePeriods.Remove(ip);
            await _context.SaveChangesAsync();
            return ip;
        }
        public bool IneligiblePeriodExists(int id)
        {
            return _context.IneligiblePeriods.Any(e => e.Id == id);
        }


    }

}
