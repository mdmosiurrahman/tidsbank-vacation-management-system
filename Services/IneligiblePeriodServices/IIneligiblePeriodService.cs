﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.IneligiblePeriods;

namespace TidsbankenFinalProject.Services.IneligiblePeriodServices
{
    public interface IIneligiblePeriodService
    {
        public Task<IEnumerable<IneligiblePeriod>> GetAllIneligiblePeriodAsync();
        public Task<IneligiblePeriod> GetIneligiblePeriodByIdAsync(int ipId);
        public Task<IneligiblePeriod> CreateIneligiblePeriodAsync(IneligiblePeriod ip);
        public Task<IneligiblePeriod> UpdateIneligiblePeriodAsync(IneligiblePeriod ip);
        public Task<IneligiblePeriod> DeleteIneligiblePeriodAsync(int Id);
        public bool IneligiblePeriodExists(int id);

    }
}
