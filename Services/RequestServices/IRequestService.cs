﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Request;

namespace TidsbankenFinalProject.Services.RequestServices
{
    public interface IRequestService
    {
        public Task<IEnumerable<Request>> GetAllRequestsAsync();
        public Task<Request> GetByIdAsync(int requestId);
        public Task<Request> UpdateAsync(Request request);
        public Task<Request> SaveRequestAsync(Request request);
        public Task<Request> DeleteByIdAsync(int requestId);
        public bool RequestExists(int id);
        public Task<IEnumerable<Request>> GetRequestByUserIdAsync(string UserId);


    }
}