﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Request;
using static TidsbankenFinalProject.Models.Request;

namespace TidsbankenFinalProject.Services.RequestServices
{

    public class RequestService : IRequestService
    {
        private readonly TidsBankDBContext _context;


        public RequestService(TidsBankDBContext context)
        {
            _context = context;


        }

        public async Task<IEnumerable<Request>> GetAllRequestsAsync()
        {
            return await _context.Requests.Include(i => i.Owner).ToArrayAsync();

        }

        public async Task<Request> GetByIdAsync(int requestId)
        {
            return await _context.Requests.SingleOrDefaultAsync(u => u.Id == requestId);
        }

        public async Task<Request> UpdateAsync(Request request)
        {
            _context.Entry(request).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return request;
        }

        public async Task<Request> SaveRequestAsync(Request request)
        {
            await _context.Requests.AddAsync(request);
            await _context.SaveChangesAsync();
            return request;
        }

        public async Task<Request> DeleteByIdAsync(int requestId)
        {
            var request = await GetByIdAsync(requestId);
            if (request == null) return null;

            _context.Requests.Remove(request);
            await _context.SaveChangesAsync();
            return request;
        }

        public async Task<IEnumerable<Request>> GetRequestByUserIdAsync(string UserId)
        {
            return await _context.Requests.Include(i => i.Owner).Where(r => r.OwnerId == UserId).ToListAsync();
        }

        public bool RequestExists(int id)
        {
            return _context.Requests.Any(e => e.Id == id);
        }
    }
}