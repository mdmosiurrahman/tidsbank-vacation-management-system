﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.User;

namespace TidsbankenFinalProject.Services.UserServices
{
    public interface IUserService
    {

        // public Task<User> CurrentUserAsync();
        public Task<IEnumerable> GetAllUsersAsync();
        public Task<User> UpdateAsync(User user);
        public Task<User> DeleteByIdAsync(string userId);
        public Task<User> GetByIdAsync(string userId);
        public Task<User> GetByEmailAsync(string email);
        public Task<User> SaveUserAsync(User user);
        public bool UserExist(string id);
        public bool IsAdmin(string id);



    }
}
