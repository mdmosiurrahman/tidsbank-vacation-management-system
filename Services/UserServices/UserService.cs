﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.User;

namespace TidsbankenFinalProject.Services.UserServices
{
    public class UserService : IUserService
    {


        private readonly TidsBankDBContext _context;



        public UserService(TidsBankDBContext context)
        {

            _context = context;
        }



        public async Task<User> GetByEmailAsync(string email) => await _context.Users.SingleOrDefaultAsync(u => u.Email == email);
        public async Task<User> GetByIdAsync(string userId) => await _context.Users.SingleOrDefaultAsync(u => u.Id == userId);

        public async Task<User> DeleteByIdAsync(string userId)
        {
            var user = await GetByIdAsync(userId);
            if (user == null) return null;

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<IEnumerable> GetAllUsersAsync()
        {
            return await _context.Users.ToArrayAsync();

        }

        public async Task<User> UpdateAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return user;

        }
        public async Task<User> SaveUserAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return user;
        }
        public bool UserExist(string id)
        {
            return _context.Users.Any(u => u.Id == id);
        }
        public bool IsAdmin(string id)
        {
            return _context.Users.Any(u => u.Id == id);
        }


    }
}
