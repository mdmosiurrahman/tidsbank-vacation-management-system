using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TidsbankenFinalProject.Models;
using AutoMapper;
using TidsbankenFinalProject.Models.DTOs.User;

namespace TidsbankenFinalProject.Services.CommentServices
{
    public class CommentService : ICommentService
    {
        private TidsBankDBContext _context;
        private readonly IMapper _mapper;


        public CommentService(TidsBankDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Comment>> GetAllCommentsAsync()
        {
            return await _context.Comments.ToArrayAsync();
        }

        public async Task<Comment> EditCommentAsync(Comment comment)
        {
            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return comment;
        }

        public async Task<Comment> CreateComment(Comment comment)
        {
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
            return comment;
        }
        public async Task<Comment> GetCommentsByIdAsync(int Id) => await _context.Comments.SingleOrDefaultAsync(c => c.Id == Id);

        public async Task<Comment> DeleteCommentByIdAsync(int Id)
        {
            var comment = await GetCommentsByIdAsync(Id);
            if (comment == null) return null;

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            return comment;
        }

        public async Task<List<Comment>> GetCommentsByrequestIdAsync(int RequestId)
        {
            return await _context.Comments.Where(c => c.RequestId == RequestId).OrderBy(x => x.CreatedOn).ToListAsync();
        }

        public bool CommentExist(int id)
        {
            return _context.Comments.Any(c => c.Id == id);
        }

        public async Task<Comment> GetCommentByRequestIdAndCommentId(int RequestId, int CommentId)
        {
            return await _context.Comments.SingleOrDefaultAsync(c => c.Id == CommentId && c.RequestId == RequestId);
        }

    }
}
