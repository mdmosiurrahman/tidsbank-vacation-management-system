using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.User;

namespace TidsbankenFinalProject.Services.CommentServices
{
    public interface ICommentService
    {
        public Task<IEnumerable<Comment>> GetAllCommentsAsync();
        public Task<Comment> EditCommentAsync(Comment comment);
        public Task<Comment> CreateComment(Comment comment);
        public Task<Comment> GetCommentsByIdAsync(int Id);
        public Task<Comment> DeleteCommentByIdAsync(int Id);
        public bool CommentExist(int id);
        public Task<List<Comment>> GetCommentsByrequestIdAsync(int RequestId);
        public Task<Comment> GetCommentByRequestIdAndCommentId(int RequestId, int CommentId);


    }
}
