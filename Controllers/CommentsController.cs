using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Comment;
using TidsbankenFinalProject.Services.CommentServices;
using TidsbankenFinalProject.Services.RequestServices;

namespace TidsbankenFinalProject.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        // private readonly TidsBankDBContext _context;
        private readonly IMapper _mapper;
        private readonly ICommentService _commentService;
        private readonly IRequestService _requestService;

        public CommentsController(IMapper mapper, ICommentService commentService, IRequestService requestService)
        {

            _mapper = mapper;
            _commentService = commentService;
            _requestService = requestService;
        }

        // GET: api/Comments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadCommentDto>>> GetComments()
        {
            return _mapper.Map<List<ReadCommentDto>>(await _commentService.GetAllCommentsAsync());
        }

        // GET: api/Comments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadCommentDto>> GetComments(int id)
        {
            var comment = await _commentService.GetCommentsByIdAsync(id);

            if (comment == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadCommentDto>(comment);
        }

        // PUT: api/Comments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComment(int id, EditCommentDto dtoComment)
        {
            if (id != dtoComment.Id)
            {
                return BadRequest();
            }
            if (!_commentService.CommentExist(id))
            {
                return NotFound();
            }

            //Map to domain
            Comment domainComment = _mapper.Map<Comment>(dtoComment);

            await _commentService.EditCommentAsync(domainComment);

            return NoContent();
        }

        // POST: api/Comments
        [HttpPost]
        public async Task<ActionResult<ReadCommentDto>> PostComment(CreateCommentDto dtoComment)
        {
            Comment domainComment = _mapper.Map<Comment>(dtoComment);
            domainComment = await _commentService.CreateComment(domainComment);
            return CreatedAtAction("GetComments", new { Id = domainComment.Id },
                  _mapper.Map<CreateCommentDto>(dtoComment));
        }

        // DELETE: api/Comments/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteComment(int id)
        {
            if (!_commentService.CommentExist(id))
            {
                return NotFound();
            }

            await _commentService.DeleteCommentByIdAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Get all comments associated with the specified request
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [Route("/{requestId}/comment")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadCommentDto>>> GetCommentByRequestId(int requestId)
        {
            return _mapper.Map<List<ReadCommentDto>>(await _commentService.GetCommentsByrequestIdAsync(requestId));
        }

        /// <summary>
        /// Create a new comment By RequestId
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="dtoComment"></param>
        /// <returns></returns>
        // POST: api/Comments
        [Route("/{requestId}/comment")]
        [HttpPost]
        public async Task<ActionResult<ReadCommentDto>> PostComments(int requestId, CreateCommentDto dtoComment)
        {
            Comment domainComment = _mapper.Map<Comment>(dtoComment);
            domainComment.RequestId = requestId;
            domainComment = await _commentService.CreateComment(domainComment);
            return CreatedAtAction("GetComments", new { Id = domainComment.Id },
                  _mapper.Map<CreateCommentDto>(dtoComment));
        }

        /// <summary>
        /// Returns the comment corresponding to the Given CommentId and RequestId
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="commentId"></param>
        /// <returns></returns>
        [Route("request/{requestId}/comment/{commentId}")]
        [HttpGet]
        public async Task<ActionResult<ReadCommentDto>> GetCommentByCommentAndRequestId(int requestId, int commentId)
        {
            if (!_commentService.CommentExist(commentId) || !_requestService.RequestExists(requestId))
            {
                return NotFound();
            }
            return _mapper.Map<ReadCommentDto>(await _commentService.GetCommentByRequestIdAndCommentId(requestId, commentId));
        }

        /// <summary>
        /// Partially updates the Comment corresponding to the provided request ID and comment Id
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="dtoComment"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [Route("/{requestId}/comment/{commentId}")]
        [HttpPatch]
        public async Task<IActionResult> EditCommentByRequestId(int commentId, EditCommentDto dtoComment, int requestId)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null)
            {
                return ResponseCodes.Create(status: 404);
            }
            var comment = await _commentService.GetCommentByRequestIdAndCommentId(requestId, commentId);
            if (comment == null)
            {
                return ResponseCodes.Create(status: 404);
            }

            if (comment.CreatedOn.AddDays(1) < DateTime.UtcNow)
            {
                ModelState.AddModelError("", "You cannot edit comments after 24 hours");
                return ResponseCodes.WithModelState(modelState: ModelState, status: 403);
            }

            comment = _mapper.Map(dtoComment, comment);
            comment = await _commentService.EditCommentAsync(comment);
            return ResponseCodes.Create(_mapper.Map<ReadCommentDto>(comment));
        }


        /// <summary>
        /// Deletes the specific comment , corrosponding to the given requestId
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="requestId"></param> 
        /// <returns></returns>
        [Route("/request/{requestId}/comment/{commentId}")]
        [HttpDelete]
        public async Task<ActionResult> DeleteSpecificCommentByRequestId(int commentId, int requestId)
        {
            var comment = await _commentService.GetCommentByRequestIdAndCommentId(requestId, commentId);
            if (comment == null)
            {
                return ResponseCodes.Create(status: 404);
            }

            //if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != comment.UserId)
            //{
            //    ModelState.AddModelError("", "Only admins can delete other users comments");
            //    return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            //}

            if (comment.CreatedOn.AddDays(1) < DateTime.UtcNow)
            {
                ModelState.AddModelError("", "You cannot delete comments after 24 hours");
                return ResponseCodes.WithModelState(modelState: ModelState, status: 403);
            }

            await _commentService.DeleteCommentByIdAsync(commentId);
            return ResponseCodes.Create(_mapper.Map<ReadCommentDto>(comment));

        }

    }
}
