﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.IneligiblePeriods;
using TidsbankenFinalProject.Models.DTOs.Request;
using TidsbankenFinalProject.Services.IneligiblePeriodServices;

namespace TidsbankenFinalProject.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class IneligiblePeriodsController : ControllerBase
    {

        private readonly IIneligiblePeriodService _ineligiblePeriodService;
        private readonly IMapper _mapper;

        public IneligiblePeriodsController(IIneligiblePeriodService ineligiblePeriodService, IMapper mapper)
        {

            _ineligiblePeriodService = ineligiblePeriodService;
            _mapper = mapper;
        }


        /// <summary>
        /// Returns List of Ineligible Periods
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadIneligiblePeriodDto>>> GetIneligiblePeriods()
        {
            return _mapper.Map<List<ReadIneligiblePeriodDto>>(await _ineligiblePeriodService.GetAllIneligiblePeriodAsync());
        }

        /// <summary>
        /// Returns Specific Ineligible Period
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadIneligiblePeriodDto>> GetIneligiblePeriod(int id)
        {
            IneligiblePeriod domainIp = await _ineligiblePeriodService.GetIneligiblePeriodByIdAsync(id);
            if (domainIp == null)
            {
                return NotFound();
            }
            return _mapper.Map<ReadIneligiblePeriodDto>(domainIp);
        }

        /// <summary>
        /// Edits the Ineligibleperiod by Id Partially
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ineligiblePeriod"></param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PutIneligiblePeriod(int id, UpdateIneligiblePeriodDto ineligiblePeriod)
        {
            if (id != ineligiblePeriod.Id)
            {
                return BadRequest();
            }

            IneligiblePeriod ineligible = _mapper.Map<IneligiblePeriod>(ineligiblePeriod);

            await _ineligiblePeriodService.UpdateIneligiblePeriodAsync(ineligible);

            return NoContent();
        }

        /// <summary>
        ///  Creates new Ineligible Period
        /// </summary>
        /// <param name="ineligible"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<IneligiblePeriod>> PostIneligiblePeriod(CreateIneligiblePeriodDto ineligible)
        {
            IneligiblePeriod ineligiblePeriod = _mapper.Map<IneligiblePeriod>(ineligible);
            ineligiblePeriod = await _ineligiblePeriodService.CreateIneligiblePeriodAsync(ineligiblePeriod);
            return CreatedAtAction("GetIneligiblePeriod", new { Id = ineligiblePeriod.Id },
                _mapper.Map<CreateIneligiblePeriodDto>(ineligible));
        }

        /// <summary>
        /// Delets the specific IP by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIneligiblePeriod(int id)
        {
            if (!_ineligiblePeriodService.IneligiblePeriodExists(id))
            {
                return NotFound();
            }
            await _ineligiblePeriodService.DeleteIneligiblePeriodAsync(id);
            return NoContent();
        }
    }
}
