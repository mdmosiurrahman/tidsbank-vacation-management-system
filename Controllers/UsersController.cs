﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs;
using TidsbankenFinalProject.Models.DTOs.Request;
using TidsbankenFinalProject.Models.DTOs.User;
using TidsbankenFinalProject.Services.RequestServices;
using TidsbankenFinalProject.Services.UserServices;

namespace TidsbankenFinalProject.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IRequestService _requestService;


        public UsersController(IMapper mapper, IUserService userService, IRequestService requestService)
        {
            _requestService = requestService;
            _userService = userService;
            _mapper = mapper;
        }
        /// <summary>
        /// Get All users 
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDto>>> GetUsers()
        {
            return _mapper.Map<List<UserReadDto>>(await _userService.GetAllUsersAsync());
        }

        /// <summary>
        /// Get USer By given Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserReadDto>> GetUser(string id)
        {
            User user = await _userService.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return _mapper.Map<UserReadDto>(user);
        }

        // GET: api/Users/abc@abc.com
        [HttpGet("api/{email}")]
        public async Task<ActionResult<UserReadDto>> GetUserByEmail(string email)
        {
            User user = await _userService.GetByEmailAsync(email);
            if (user == null)
            {
                return NotFound();
            }
            return _mapper.Map<UserReadDto>(user);
        }


        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(string id, UserEditDTO dtoUser)
        {
            if (id != dtoUser.Id)
            {
                return BadRequest();
            }
            if (!_userService.UserExist(id))
            {
                return NotFound();
            }
            //Map to domain
            User domainUser = _mapper.Map<User>(dtoUser);

            await _userService.UpdateAsync(domainUser);

            return NoContent();
        }

        /// <summary>
        /// Register a new USer (Admin only)
        /// </summary>
        /// <param name="dtoUser"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<UserCreateDto>> PostUser(UserCreateDto dtoUser)
        {

            User domainUser = _mapper.Map<User>(dtoUser);
            domainUser = await _userService.SaveUserAsync(domainUser);
            return CreatedAtAction("GetUser", new { Id = domainUser.Id },
                  _mapper.Map<UserCreateDto>(dtoUser));
        }

        /// <summary>
        /// Delete USer By Id (admin Only)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if (!_userService.UserExist(id))
            {
                return NotFound();
            }

            await _userService.DeleteByIdAsync(id);

            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> EditUser(string id, UserEditDTO dtoUser)
        {
            if (id != dtoUser.Id)
            {
                return BadRequest();
            }
            if (!_userService.UserExist(id))
            {
                return NotFound();
            }

            User domainUser = _mapper.Map<User>(dtoUser);

            await _userService.UpdateAsync(domainUser);

            return NoContent();
        }
        /// <summary>
        /// Get Requests Belong to the given UseID
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{userId}/requests")]
        public async Task<ActionResult<IEnumerable<RequestViewDto>>> GetRequestByUserId(string userId)
        {

            return _mapper.Map<List<RequestViewDto>>(await _requestService.GetRequestByUserIdAsync(userId));
        }

    }
}
