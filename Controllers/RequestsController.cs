﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Request;
using TidsbankenFinalProject.Services.RequestServices;

namespace TidsbankenFinalProject.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RequestsController : ControllerBase
    {
        private readonly TidsBankDBContext _context;
        private readonly IRequestService _requestService;
        private readonly IMapper _mapper;

        public RequestsController(TidsBankDBContext context, IRequestService requestService, IMapper mapper)
        {
            _context = context;
            _requestService = requestService;
            _mapper = mapper;
        }

        // GET: api/Requests
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RequestViewDto>>> GetRequests()
        {
            return _mapper.Map<List<RequestViewDto>>(await _requestService.GetAllRequestsAsync());

        }

        // GET: api/Requests/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RequestViewDto>> GetRequest(int id)
        {
            Request request = await _requestService.GetByIdAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            return _mapper.Map<RequestViewDto>(request);
        }

        // PUT: api/Requests/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRequest(int id, UpdateRequestDto request)
        {
            if (id != request.Id)
            {
                return BadRequest();
            }

            Request domainUser = _mapper.Map<Request>(request);

            await _requestService.UpdateAsync(domainUser);

            return NoContent();
        }

        // POST: api/Requests
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Request>> PostRequest(CreateRequestDto request)
        {
            Request domainUser = _mapper.Map<Request>(request);
            domainUser = await _requestService.SaveRequestAsync(domainUser);
            return CreatedAtAction("GetRequest", new { Id = domainUser.Id },
                _mapper.Map<CreateRequestDto>(request));
        }

        // DELETE: api/Requests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRequest(int id)
        {
            var request = await _context.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }

            _context.Requests.Remove(request);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [Route("User/{userId}/request")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RequestViewDto>>> GetRequestByUserId(string userId)
        {

            return _mapper.Map<List<RequestViewDto>>(await _requestService.GetRequestByUserIdAsync(userId));
        }

    }
}
