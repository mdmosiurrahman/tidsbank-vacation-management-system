using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.SettingsDTO;
using TidsbankenFinalProject.Services.SettingServices;

namespace TidsbankenFinalProject.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        private readonly ISettingService _settingService;
        private readonly IMapper _mapper;
        public SettingsController(ISettingService settingService, IMapper mapper)
        {

            _settingService = settingService;
            _mapper = mapper;
        }

        // GET: api/Settings
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SettingsReadDTO>>> GetSettings()
        {
            return _mapper.Map<List<SettingsReadDTO>>(await _settingService.GetAllSettingsAsync());
        }

        // GET: api/Settings/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SettingsReadDTO>> GetSetting(int id)
        {
            Setting setting = await _settingService.GetSettingsByIdAsync(id);
            if (setting == null)
            {
                return NotFound();
            }
            return _mapper.Map<SettingsReadDTO>(setting);


        }

        // PUT: api/Settings/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSetting(int id, SettingsUpdateDTO dtoSetting)
        {
            if (id != dtoSetting.Id)
            {
                return BadRequest();
            }
            if (!_settingService.SettingExist(id))
            {
                return NotFound();
            }
            //Map to domain
            Setting domainSetting = _mapper.Map<Setting>(dtoSetting);

            await _settingService.EditSettingsAsync(domainSetting);

            return NoContent();
        }

        // POST: api/Settings
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SettingsReadDTO>> PostSetting(SettingCreateDTO dtoSetting)
        {
            Setting domainSetting = _mapper.Map<Setting>(dtoSetting);
            domainSetting = await _settingService.CreateSetting(domainSetting);
            return CreatedAtAction("GetSetting", new { Id = domainSetting.Id },
                  _mapper.Map<SettingCreateDTO>(dtoSetting));
        }

        // DELETE: api/Settings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSetting(int id)
        {
            if (!_settingService.SettingExist(id))
            {
                return NotFound();
            }

            await _settingService.DeleteSettingsByIdAsync(id);

            return NoContent();
        }


    }
}
