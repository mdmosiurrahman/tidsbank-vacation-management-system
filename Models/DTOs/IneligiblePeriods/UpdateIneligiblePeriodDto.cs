﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.IneligiblePeriods
{
    public class UpdateIneligiblePeriodDto
    {
        public int Id { get; set; }
        /// <summary>
        /// The start of the ineligible period.
        /// </summary>
        public DateTime? Start { get; set; } = null;
        /// <summary>
        /// The end of the ineligible period.
        /// </summary>
        public DateTime? End { get; set; } = null;
        public int UserId { get; set; }
    }
}
