﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.IneligiblePeriods
{
    public class CreateIneligiblePeriodDto
    {
        /// <summary>
        /// The start of the ineligible period.
        /// </summary>
        [Required]
        public DateTime? Start { get; set; }
        /// <summary>
        /// The end of the ineligible period.
        /// </summary>
        [Required]
        public DateTime? End { get; set; }
        public int UserId { get; set; }
    }
}
