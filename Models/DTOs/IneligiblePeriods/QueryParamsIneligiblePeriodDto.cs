﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.IneligiblePeriods
{
    public class QueryParamsIneligiblePeriodDto
    {
        /// <summary>
        /// Query periods from
        /// </summary>
        public DateTime Start { get; set; } = DateTime.MinValue;
        /// <summary>
        /// Query periods to
        /// </summary>
        public DateTime End { get; set; } = DateTime.MaxValue;
    }
}
