﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.SettingsDTO
{
    public class SettingsUpdateDTO
    {
        public int Id { get; set; }

        [Range(1, int.MaxValue)]
        public int MaxVacationLength { get; set; }
    }
}
