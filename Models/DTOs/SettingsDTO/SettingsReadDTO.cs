﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.SettingsDTO
{
    public class SettingsReadDTO
    {
        public int Id { get; set; }
        public int MaxVacationLength { get; set; }
    }
}
