﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.Comment
{
    public class ReadCommentDto
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        //RElation
        public int RequestId { get; set; }
        public string UserId { get; set; }
        //  public string UsernName { get; set; }

    }
}
