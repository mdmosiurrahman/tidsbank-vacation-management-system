﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.Comment
{
    public class QueryCommentDto
    {
        /// <summary>
        /// Default 0
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Default 20
        /// </summary>
        [Range(1, int.MaxValue)]
        public int Limit { get; set; } = 20;
    }
}
