﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.Comment
{
    public class CreateCommentDto
    {

        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int RequestId { get; set; }
        public string UserId { get; set; }

    }
}
