﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.Request
{
    public class CreateRequestDto
    {
        /// <summary>
        /// The title of the request
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        /// <summary>
        /// The start of the request period.
        /// </summary>
        [Required]
        public DateTime? PeriodStart { get; set; }
        /// <summary>
        /// The end of the request period.
        /// </summary>
        [Required]
        public DateTime? PeriodEnd { get; set; }
    }
}
