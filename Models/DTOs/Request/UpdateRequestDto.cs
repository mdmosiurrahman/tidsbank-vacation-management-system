﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.Request
{
    public class UpdateRequestDto
    {
        public int Id { get; set; }
        /// <summary>
        /// The title of the request
        /// </summary>
        [StringLength(50)]
        public string Title { get; set; } = null;
        /// <summary>
        /// The start of the request period.
        /// </summary>
        public DateTime? PeriodStart { get; set; } = null;
        /// <summary>
        /// The end of the request period.
        /// </summary>
        public DateTime? PeriodEnd { get; set; } = null;
        public DateTime? ModeratedAt { get; set; }
        public string State { get; set; } = null;
        public string ModeratorId { get; set; }
        public string OwnerId { get; set; }
      
    }
}
