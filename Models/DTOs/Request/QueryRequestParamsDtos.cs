﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.Request
{
    public class QueryRequestParamsDtos
    {
        /// <summary>
        /// Default 0
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Default 50
        /// </summary>
        [Range(1, int.MaxValue)]
        public int Limit { get; set; } = 50;
        /// <summary>
        /// Query periods from.
        /// </summary>
        public DateTime Start { get; set; } = DateTime.MinValue;
        /// <summary>
        /// Query periods to.
        /// </summary>
        public DateTime End { get; set; } = DateTime.MaxValue;

    }
}
