﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models
{
    /// <summary>
    /// The common return type of all end points
    /// </summary>
    public class ResponseCodes
    {
        /// <summary>
        /// The status of the response 
        /// </summary>
        public int Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; } = null;
        public IDictionary<string, string[]> Errors { get; set; } = null;

        public ResponseCodes(int status = 200)
        {
            Status = status;
            Message = ReasonPhrases.GetReasonPhrase(status);
        }
        public ResponseCodes(ModelStateDictionary modelState, int status = 200) : this(new ValidationProblemDetails(modelState), status) { }
        public ResponseCodes(ValidationProblemDetails problemDetails, int status = 200)
        {
            Status = status;
            Message = ReasonPhrases.GetReasonPhrase(status);
            Errors = problemDetails?.Errors;
        }
        public static ObjectResult Create(object data = null, int status = 200)
        {
            return new ObjectResult(new ResponseCodes(status) { Data = data }) { StatusCode = status };
        }
        public static ObjectResult WithErrors(object data = null, IDictionary<string, string[]> errors = null, int status = 400)
        {
            return new ObjectResult(new ResponseCodes(status) { Data = data, Errors = errors }) { StatusCode = status };
        }
        public static ObjectResult WithModelState(object data = null, ModelStateDictionary modelState = null, int status = 400)
        {
            return new ObjectResult(new ResponseCodes(modelState, status) { Data = data }) { StatusCode = status };
        }
        public static ObjectResult Error401(object data = null, ModelStateDictionary modelState = null, int status = 401)
        {
            return new ObjectResult(new ResponseCodes(modelState, status) { Data = data }) { StatusCode = status };
        }
        public static ObjectResult Error500(object data = null, ModelStateDictionary modelState = null, int status = 500)
        {
            return new ObjectResult(new ResponseCodes(modelState, status) { Data = data }) { StatusCode = status };
        }
    }
}
