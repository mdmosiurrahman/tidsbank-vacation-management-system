﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.User
{
    public class UserLimitedView
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
    }
}
