﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.User
{
    public class UserResetPasswDto
    {
        [Required]
        public string Password1 { get; set; }
        [Required]
        public string Password2 { get; set; }
    }
}
