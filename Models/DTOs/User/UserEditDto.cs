﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs
{
    public class UserEditDTO
    {
        public string Id { get; set; }

        public bool? IsAdmin { get; set; } = null;
        public string Email { get; set; } = null;
        /// <summary>
        /// Picture url
        /// </summary>        
        public string Picture { get; set; } = null;
        public string FirstName { get; set; } = null;
        public string LastName { get; set; } = null;
    }
}
