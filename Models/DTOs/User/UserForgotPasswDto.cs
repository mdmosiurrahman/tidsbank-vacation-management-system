﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFinalProject.Models.DTOs.User
{
    public class UserForgotPasswDto
    {
        [StringLength(200)]
        [Required]
        public string Email { get; set; }
    }
}
