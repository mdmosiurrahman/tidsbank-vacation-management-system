﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TidsbankenFinalProject.Models
{
    public partial class TidsBankDBContext : DbContext
    {
        public TidsBankDBContext()
        {
        }

        public TidsBankDBContext(DbContextOptions<TidsBankDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<IneligiblePeriod> IneligiblePeriods { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasIndex(e => e.RequestId, "IX_Comments_RequestId");

                entity.HasIndex(e => e.UserId, "IX_Comments_UserId");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(512);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.RequestId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<IneligiblePeriod>(entity =>
            {
                entity.HasIndex(e => e.UserId, "IX_IneligiblePeriods_UserId");

                entity.Property(e => e.UserId).HasMaxLength(50);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.IneligiblePeriods)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Request>(entity =>
            {
                entity.HasIndex(e => e.ModeratorId, "IX_Requests_ModeratorId");

                entity.HasIndex(e => e.OwnerId, "IX_Requests_OwnerId");

                entity.Property(e => e.ModeratorId).HasMaxLength(50);

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Moderator)
                    .WithMany(p => p.RequestModerators)
                    .HasForeignKey(d => d.ModeratorId);

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.RequestOwners)
                    .HasForeignKey(d => d.OwnerId);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(50);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(320);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Picture).HasMaxLength(60);
            });

            OnModelCreatingPartial(modelBuilder);
        }


        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
