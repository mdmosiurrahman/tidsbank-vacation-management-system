﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TidsbankenFinalProject.Models
{
    public partial class Setting
    {
        public int Id { get; set; }
        public int MaxVacationLength { get; set; }
    }
}
