﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TidsbankenFinalProject.Models
{
    public partial class User
    {
        public User()
        {
            Comments = new HashSet<Comment>();
            IneligiblePeriods = new HashSet<IneligiblePeriod>();
            RequestModerators = new HashSet<Request>();
            RequestOwners = new HashSet<Request>();
        }

        public string Id { get; set; }
        public bool IsAdmin { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<IneligiblePeriod> IneligiblePeriods { get; set; }
        public virtual ICollection<Request> RequestModerators { get; set; }
        public virtual ICollection<Request> RequestOwners { get; set; }
    }
}
