﻿using System;
using System.Collections.Generic;
using System.Linq;

#nullable disable

namespace TidsbankenFinalProject.Models
{
    public partial class Request
    {
        public static class RequestStates
        {
            public const string Pending = "Pending";
            public const string Approved = "Approved";
            public const string Denied = "Denied";

            private static IEnumerable<System.Reflection.FieldInfo> StateFields()
            {
                return typeof(RequestStates).GetFields();
            }

            public static string[] GetStates()
            {
                return StateFields().Select(s => (string)s.GetRawConstantValue()).ToArray();
            }

            public static bool IsValid(string state)
            {
                return StateFields().Any(f => (string)f.GetRawConstantValue() == state);
            }
        }
        public Request()
        {
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public string State { get; set; }
        public string OwnerId { get; set; }
        public DateTime? ModeratedAt { get; set; }
        public string ModeratorId { get; set; }

        public virtual User Moderator { get; set; }
        public virtual User Owner { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
