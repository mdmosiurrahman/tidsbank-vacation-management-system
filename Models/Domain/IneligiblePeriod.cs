﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TidsbankenFinalProject.Models
{
    public partial class IneligiblePeriod
    {
        public int Id { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string UserId { get; set; }

        public virtual User User { get; set; }
    }
}
