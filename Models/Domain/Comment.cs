﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TidsbankenFinalProject.Models
{
    public partial class Comment
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int RequestId { get; set; }
        public string UserId { get; set; }
        public Request Request { get; set; }
        public User User { get; set; }
    }
}
