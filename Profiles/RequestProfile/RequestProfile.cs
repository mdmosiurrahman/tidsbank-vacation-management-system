﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Request;
using AutoMapper;

namespace TidsbankenFinalProject.Profiles
{
    public class RequestProfile : Profile
    {
        public RequestProfile()
        {
            CreateMap<Request, CreateRequestDto>().ReverseMap();
            CreateMap<Request, QueryRequestParamsDtos>().ReverseMap();
            CreateMap<Request, RequestViewDto>()
                 .ForMember(rdto => rdto.OwnerName, opt => opt
            .MapFrom(r => r.Owner.FirstName)).ReverseMap();
            CreateMap<Request, UpdateRequestDto>()
                .ReverseMap();

        }

    }
}
