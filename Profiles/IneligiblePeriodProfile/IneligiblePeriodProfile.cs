﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.IneligiblePeriods;
using TidsbankenFinalProject.Models.DTOs.Request;

namespace TidsbankenFinalProject.Profiles.IneligiblePeriodProfile
{
    public class IneligiblePeriodProfile : Profile
    {
        public IneligiblePeriodProfile()
        {
            CreateMap<IneligiblePeriod, CreateIneligiblePeriodDto>()
                .ForMember(idto => idto.UserId, opt => opt
             .MapFrom(i => i.UserId))
                .ReverseMap();

            CreateMap<IneligiblePeriod, QueryParamsIneligiblePeriodDto>().ReverseMap();
            CreateMap<IneligiblePeriod, ReadIneligiblePeriodDto>().
                ForMember(idto => idto.CretedBy, opt => opt
            .MapFrom(i => i.User.FirstName)).ReverseMap();

            CreateMap<IneligiblePeriod, UpdateIneligiblePeriodDto>().ReverseMap();
        }
    }
}
