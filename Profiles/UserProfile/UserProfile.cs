﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.User;
using AutoMapper;
using TidsbankenFinalProject.Models.DTOs;

namespace TidsbankenFinalProject.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDto>().ReverseMap();
            CreateMap<User, UserLimitedView>().ForMember(x => x.Name, src => src.MapFrom(u => u.ToString()));

            CreateMap<UserCreateDto, User>().ReverseMap();
            CreateMap<UserEditDTO, User>().ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => sourceMember != null));




        }
    }
}

