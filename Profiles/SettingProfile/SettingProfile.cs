﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Comment;
using TidsbankenFinalProject.Models.DTOs.SettingsDTO;

namespace TidsbankenFinalProject.Profiles.SettingProfile
{
    public class SettingProfile : Profile
    {
        public SettingProfile()
        {
            CreateMap<Setting, SettingsReadDTO>().ReverseMap();
            CreateMap<Setting, SettingsUpdateDTO>().ReverseMap();
            CreateMap<Setting, SettingCreateDTO>().ReverseMap();
        }
    }
}
