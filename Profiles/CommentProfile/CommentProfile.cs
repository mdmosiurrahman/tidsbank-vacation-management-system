﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Models.DTOs.Comment;
using TidsbankenFinalProject.Models.DTOs.Request;

namespace TidsbankenFinalProject.Profiles.CommentProfile
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMap<Comment, ReadCommentDto>().ReverseMap();
            CreateMap<Comment, CreateCommentDto>()
               .ForMember(cdto => cdto.RequestId, opt => opt.MapFrom(c => c.RequestId))
               .ForMember(cdto => cdto.UserId, opt => opt.MapFrom(c => c.UserId))
                .ReverseMap();
            CreateMap<Comment, QueryCommentDto>().ReverseMap();
            CreateMap<Comment, EditCommentDto>().ReverseMap();

        }
    }
}
