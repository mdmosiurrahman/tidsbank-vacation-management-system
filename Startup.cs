using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using TidsbankenFinalProject.Models;
using TidsbankenFinalProject.Services.UserServices;
using TidsbankenFinalProject.Services.RequestServices;
using TidsbankenFinalProject.Services.SettingServices;
using TidsbankenFinalProject.Services.IneligiblePeriodServices;
using TidsbankenFinalProject.Services.CommentServices;





namespace TidsbankenFinalProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        //Access token for postman can be found at http://localhost:8000/#
                        //requires token from keycloak instance - location stored in secret manager
                        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
                        {
                            var client = new HttpClient();
                            var keyuri = "http://localhost:8083/auth/realms/Tidsbanken/protocol/openid-connect/certs";
                            //Retrieves the keys from keycloak instance to verify token
                            var response = client.GetAsync(keyuri).Result;
                            var responseString = response.Content.ReadAsStringAsync().Result;
                            var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                            return keys.Keys;
                        },

                        ValidIssuers = new List<string>
                        {
                            //Configuration["TokenSecrets:IssuerURI"]
                            "http://localhost:8083/auth/realms/Tidsbanken"
                        },

                        //This checks the token for a the 'aud' claim value
                        ValidAudience = "account",
                    };
                });

            services.AddControllers();
            services.AddDbContext<TidsBankDBContext>(opt =>
                opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //Injecting Automapper
            services.AddAutoMapper(typeof(Startup));

            //Injecting Services

            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(ICommentService), typeof(CommentService));
            services.AddScoped(typeof(IRequestService), typeof(RequestService));
            services.AddScoped(typeof(ISettingService), typeof(SettingService));
            services.AddScoped(typeof(IIneligiblePeriodService), typeof(IneligiblePeriodService));
            //  services.AddMemoryCache();



            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TidsbankenFinalProject", Version = "v1" });
            });
            //   services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TidsbankenFinalProject v1"));
            }
            app.UseHttpsRedirection();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
